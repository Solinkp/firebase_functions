const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

exports.createFirulifeUser = functions.https.onCall((data, _) => {
  let userId = "";
  return admin.auth().createUser({
    email: data.email,
    emailVerified: false,
    password: data.password,
    displayName: data.displayName,
  }).then((userRecord) => {
    userId = userRecord.uid;
    return admin.auth().setCustomUserClaims(userRecord.uid, {
      type: data.type
    });
  }).then(() => {
    return {
      message: `success! ${data.email} has been registered in Firulife`,
      uid: userId
    }
  }).catch(err => {
    return err;
  });
});

exports.createFirulifeAppUser = functions.https.onCall((data, _) => {
  let userId = "";
  return admin.auth().createUser({
    email: data.email,
    emailVerified: false,
    password: data.password,
    displayName: data.displayName
  }).then((userRecord) => {
    userId = userRecord.uid;
    return admin.auth().setCustomUserClaims(userRecord.uid, {
      type: 2,
      country: data.country,
      dept: data.dept
    });
  }).then(() => {
    let userObject = {
      displayName: data.displayName,
      email: data.email,
      country: data.country,
      dept: data.dept
    };
    return admin.firestore().doc('user/'+userId).set(userObject);
  }).then(() => {
    return {
      message: `success! ${data.email} has been registered in Firulife`,
      uid: userId
    }
  }).catch(err => {
    return err;
  });
});

exports.editFirulifeAppUser = functions.https.onCall((data, _) => {
  return admin.auth().updateUser(data.id, {
    displayName: data.displayName
  }).then(() => {
    return admin.auth().getUserByEmail(data.email).then((user) => {
      const currentCustomClaims = user.customClaims;
      currentCustomClaims['country'] = data.country;
      currentCustomClaims['dept'] = data.dept;
      return admin.auth().setCustomUserClaims(user.uid, currentCustomClaims);
    });
  }).then(() => {
    let userObject = {
      displayName: data.displayName,
      email: data.email,
      country: data.country,
      dept: data.dept
    };
    return admin.firestore().doc('user/'+data.id).update(userObject);
  }).then(() => {
    return {
      message: `success! ${data.displayName} has been modified`,
      uid: data.id
    }
  }).catch(err => {
    return err;
  });
});

exports.createLostAndFoundPost = functions.https.onCall((data, _) => {
  let images = [data.pictureOne];
  if(data.pictureTwo != '') {
    images.push(data.pictureTwo);
  }
  let postObject = {
    uid: data.uid,
    datePosted: data.datePosted,
    countryId: data.countryId,
    dept: data.dept,
    pictures: images,
    name: data.name,
    description: data.description,
    dayLost: data.dayLost,
    timeLost: data.timeLost,
    zoneLost: data.zoneLost,
    phone: data.phone,
    whatsapp: data.whatsapp,
    lost: data.lost,
    active: true
  };
  var docRef = admin.firestore().collection('lostfound').doc();
  return docRef.set(postObject).then(() => {
    return {
      message: 'success! post created.',
      uid: data.uid
    }
  });
});

exports.deleteLostAndFoundPost = functions.https.onCall((data, _) => {
  let userId = data.uid;
  let postId = data.id;
  let imageOneName = data.imageOne;
  let imageTwoName = data.imageTwo;
  let pathImageOne = 'lostfound/'+userId+'/'+imageOneName+'';
  let pathImageTwo = 'lostfound/'+userId+'/'+imageTwoName+'';

  return admin.firestore().doc('lostfound/'+postId).delete().then(() => {
    return admin.storage().bucket().file(pathImageOne).delete().then(() => {
      if(imageTwoName != '') {
        return admin.storage().bucket().file(pathImageTwo).delete().then(() => {
          return {
            message: 'success! post deleted.',
            pid: postId
          }
        });
      } else {
        return {
          message: 'success! post deleted.',
          pid: postId
        }
      }
    });
  });
});
